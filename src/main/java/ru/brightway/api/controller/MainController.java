package ru.brightway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.brightway.api.entity.Shedule;
import ru.brightway.api.repository.SheduleRepository;

import java.util.List;

@RestController
public class MainController {

    @Autowired
    private SheduleRepository sheduleRepository;

    @PostMapping("/shedules")
    public void updateShedule(@RequestBody Shedule shedule) {
        sheduleRepository.save(shedule);
    }

    @GetMapping("/shedules")
    public List<Shedule> showAllShedules(){
        return sheduleRepository.findAll();
    }

}
