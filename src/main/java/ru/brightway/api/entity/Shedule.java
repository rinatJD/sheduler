package ru.brightway.api.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import ru.brightway.api.repository.SheduleRepository;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "shedule")
public class Shedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type")
    private String type;

    @Column(name = "date_of_the_month")
    private int dateOfTheMonth;

    @Column(name = "month_count")
    private int monthCount;

    @Column(name = "month_needed")
    private int monthNeeded;

}
