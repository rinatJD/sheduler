package ru.brightway.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.brightway.api.entity.Shedule;

import java.util.List;

public interface SheduleRepository extends JpaRepository<Shedule, Integer> {

    List<Shedule> findShedulesByDateOfTheMonth(int date);
    List<Shedule> findShedulesByMonthNeededIsGreaterThan(int count);

}
