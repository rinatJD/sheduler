package ru.brightway.api.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.brightway.api.entity.Shedule;
import ru.brightway.api.repository.SheduleRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class SendingTask {

    @Autowired
    private SheduleRepository sheduleRepository;

    @Scheduled(cron = "0 0 1 * * *")
    public void reportCurrentTime() {

        final int MAX_MONTH_COUNT = 8;

        int year = 0;
        int currentMonth = 0;
        int monthsUploadedToday = 0;

        List<Shedule> shedules = findShedulesByMonthNeededIsGreaterThanZero(1);

        if (shedules.size() > 0) {
            LocalDateTime now = LocalDateTime.now();
            year = now.getYear();
            currentMonth = now.getMonth().getValue();
        }

        for (Shedule shedule : shedules) {

            // месяцев осталось загрузить
            int monthNeeded = shedule.getMonthNeeded();
            // месяцев отработано
            int monthWorked = shedule.getMonthCount() - monthNeeded;
            // месяц, за который нужно отправить задание на загрузку
            int monthLoaded = (currentMonth-1) - monthWorked;

            while (monthNeeded > 0 && monthsUploadedToday < MAX_MONTH_COUNT) {

                // сегодня 3 месяц, всего надо загрузить 10, загружено 7. monthLoaded == -5
                // сегодня 3 месяц, всего надо загрузить 10, загружено 2. monthLoaded == 0
                if (monthLoaded < 1) { // перешел на предыдущий год
                    monthLoaded += 12;
                    year--;
                }

                int responseCode = SendRequest(shedule, year, monthLoaded, monthNeeded, monthsUploadedToday, 1);

                if (responseCode == 200) {
                    monthLoaded--;
                    monthNeeded--;
                    monthsUploadedToday++;
                } else {
                    return;
                }

                shedule.setMonthNeeded(monthNeeded);
                saveToDB(shedule, 1);

            }

        }

    }

    private int SendRequest(Shedule shedule, int year, int monthLoaded, int monthNeeded, int monthsUploadedToday, int counter) {

        log.info(String.format("Отправляем запрос с параметрами: type = %s, year = %d, month = %d"
                , shedule.getType(), year, monthLoaded));

        RestTemplate restTemplate = new RestTemplate();

        String url = "https://d5f720c1-4c34-40bf-8b99-9f7b6063201d.mock.pstmn.io/report?" +
                "type=" + shedule.getType() +
                "&year=" + year +
                "&month=" + monthLoaded;

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.getForEntity(url, String.class);
        } catch (Exception e) {
            log.error("Ошибка при отправке запроса: " + e.getMessage());
            try {
                if (counter == 3) {
                    return 404;
                }
                Thread.sleep(3600000);
                SendRequest(shedule, year, monthLoaded, monthNeeded, monthsUploadedToday, ++counter);
            } catch (InterruptedException e1) {
                log.error("Ошибка при засыпании потока: " + e1.getMessage());
            }
        }

        if (Objects.isNull(response)) {
            return 404;
        }

        log.info(String.format("Получили код ответа: %d", response.getStatusCodeValue()));

        if (response.getStatusCodeValue() == 400 ||
                response.getStatusCodeValue() == 401) {
            try {
                if (counter == 3) {
                    return response.getStatusCodeValue();
                }
                Thread.sleep(3600000);
                SendRequest(shedule, year, monthLoaded, monthNeeded, monthsUploadedToday, ++counter);
            } catch (InterruptedException e) {
                log.error("Ошибка при засыпании потока: " + e.getMessage());
            }
        }

        return response.getStatusCodeValue();
    }

    private List<Shedule> findShedulesByMonthNeededIsGreaterThanZero(int counter) {

        log.info("Смотрим, нужно ли сегодня отправлять запросы в МДЛП");

        List<Shedule> shedules = new ArrayList<>();

        try {
            shedules = sheduleRepository.findShedulesByMonthNeededIsGreaterThan(0);
        } catch  (JDBCException e) {
            log.error("Ошибка чтения из БД." + e.getSQLState());
            try {
                if (counter == 3) {
                    return shedules;
                }
                Thread.sleep(3600000);
                shedules = findShedulesByMonthNeededIsGreaterThanZero(++counter);
            } catch (InterruptedException e1) {
                log.error("Ошибка при засыпании потока: " + e1.getMessage());
            }
            return shedules;
        }

        return shedules;
    }

    private void saveToDB(Shedule shedule, int counter) {
        log.info("Записываем изменение в БД");
        try {
            sheduleRepository.save(shedule);
        } catch (JDBCException e) {
            log.error("Ошибка записи в БД. " + e.getSQLState());
            try {
                if (counter == 3) {
                    return;
                }
                Thread.sleep(3600000);
                saveToDB(shedule, ++counter);
            } catch (InterruptedException e1) {
                log.error("Ошибка при засыпании потока: " + e1.getMessage());
            }
        }
    }


}
