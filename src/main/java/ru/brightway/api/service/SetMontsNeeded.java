package ru.brightway.api.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.SQLErrorCodes;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.brightway.api.entity.Shedule;
import ru.brightway.api.repository.SheduleRepository;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SetMontsNeeded {

    @Autowired
    private SheduleRepository sheduleRepository;

    @Scheduled(cron = "@daily")
    public void SetMontsNeededMethod(){

        LocalDateTime now = LocalDateTime.now();
        int currentDay = now.getDayOfMonth();

        List<Shedule> sheduleList = findShedulesByDateOfTheMonth(currentDay, 1);

        for (Shedule shedule : sheduleList) {
            shedule.setMonthNeeded(shedule.getMonthCount());
            saveToDB(shedule, 1);
        }

    }

    private List<Shedule> findShedulesByDateOfTheMonth(int currentDay, int counter) {

        log.info("Смотрим, нужно ли сегодня изменить значение month_needed");

        List<Shedule> shedules = new ArrayList<>();

        try {
            shedules = sheduleRepository.findShedulesByDateOfTheMonth(currentDay);
        } catch  (JDBCException e) {
            log.error("Ошибка чтения из БД." + e.getSQLState());
            try {
                if (counter == 3) {
                    return shedules;
                }
                Thread.sleep(3600000);
                shedules = findShedulesByDateOfTheMonth(currentDay, ++counter);
            } catch (InterruptedException e1) {
                log.error("Ошибка при засыпании потока: " + e1.getMessage());
            }
            return shedules;
        }

        return shedules;
    }

    private void saveToDB(Shedule shedule, int counter) {
        log.info("Записываем изменение в БД");
        try {
            sheduleRepository.save(shedule);
        } catch (JDBCException e) {
            log.error("Ошибка записи в БД. " + e.getSQLState());
            try {
                if (counter == 3) {
                    return;
                }
                Thread.sleep(3600000);
                saveToDB(shedule, ++counter);
            } catch (InterruptedException e1) {
                log.error("Ошибка при засыпании потока: " + e1.getMessage());
            }
        }
    }

}
