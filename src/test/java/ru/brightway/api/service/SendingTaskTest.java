package ru.brightway.api.service;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.brightway.api.entity.Shedule;
import ru.brightway.api.repository.SheduleRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SendingTaskTest {

    @Autowired
    SheduleRepository sheduleRepository;

    RestTemplate restTemplate = new RestTemplate();

    Shedule shedule;

    @BeforeAll
    void setUp() {
        shedule = new Shedule();
        shedule.setMonthNeeded(2);
        shedule.setMonthCount(2);
        shedule.setDateOfTheMonth(99);
        shedule.setType("balance");
        sheduleRepository.save(shedule);
    }

    @Test
    void reportCurrentTime() {

        List<Shedule> shedules = sheduleRepository.findShedulesByMonthNeededIsGreaterThan(0);
        assertTrue(shedules.size() >= 1);

        String url = "https://d5f720c1-4c34-40bf-8b99-9f7b6063201d.mock.pstmn.io/report?" +
                "type=" + shedule.getType() +
                "&year=" + 2023 +
                "&month=" + 6;

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertEquals(response.getStatusCodeValue(), 200);
        assertNotEquals(response.getStatusCodeValue(), 404);

        url = "https://yandex.ru/report?" +
                "type=" + shedule.getType() +
                "&year=" + 2023 +
                "&month=" + 6;

        String finalUrl = url;

        Assertions.assertThrows(HttpClientErrorException.class, () -> {
            restTemplate.getForEntity(finalUrl, String.class);
        });

        try {
            response = restTemplate.getForEntity(url, String.class);
        } catch (HttpClientErrorException e) {
            assertEquals(e.getRawStatusCode(), 404);
        };

    }

    @AfterAll
    void after() {
        sheduleRepository.delete(shedule);
    }

}