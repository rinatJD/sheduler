package ru.brightway.api.service;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.brightway.api.entity.Shedule;
import ru.brightway.api.repository.SheduleRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SetMontsNeededTest {

    @Autowired
    SheduleRepository sheduleRepository;

    Shedule shedule;

    @BeforeAll
    void setUp() {
        shedule = new Shedule();
        shedule.setMonthNeeded(0);
        shedule.setMonthCount(2);
        shedule.setDateOfTheMonth(99);
        shedule.setType("balance");
        sheduleRepository.save(shedule);
    }

    @Test
    void setMontsNeededMethod() {

        shedule.setMonthNeeded(shedule.getMonthCount());
        sheduleRepository.save(shedule);

        List<Shedule> sheduleList = sheduleRepository.findShedulesByDateOfTheMonth(99);
        Shedule shedule1 = sheduleList.get(0);
        assertEquals(shedule1.getMonthCount(), shedule1.getMonthNeeded());

    }

    @Test
    void findShedulesByDateOfTheMonth() {

        List<Shedule> sheduleList = sheduleRepository.findShedulesByDateOfTheMonth(99);
        assertEquals(1, sheduleList.size());

        sheduleRepository.delete(shedule);

        sheduleList = sheduleRepository.findShedulesByDateOfTheMonth(99);
        assertEquals(0, sheduleList.size());

    }

}